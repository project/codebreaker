CodeBreaker Game
-----------------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation

INTRODUCTION
------------

This module provide a CodeBreaker game block.
We can configure that block on any page.
We can place multiple blocks on a single page.


INSTALLATION
------------

Use [Composer] to get module
(https://www.drupal.org/project/codebreaker).

  ```
  composer require drupal/codebreaker

 ```
