/* eslint-disable consistent-return, no-console */

( function( $ ) {
  $.fn.shake = function( intShakes, intDistance, intDuration ) {
    this.each( function() {
      $( this ).css( "position", "relative" );
      for ( let x = 1; x <= intShakes; x++ ) {
        $( this ).animate( { left: ( intDistance * -1 ) }, ( ( ( intDuration / intShakes ) / 4 ) ) )
          .animate( { left: intDistance }, ( ( intDuration / intShakes ) / 2 ) )
          .animate( { left: 0 }, ( ( ( intDuration / intShakes ) / 4 ) ) );
      }
    } );
    return this;
  };

  let ticker = false;

  $.fn.codebreaker = function( params ) {
    params = $.extend( this.defaults, params );

    function shuffle( array ) {
      let counter = array.length; let temp; let
        index;
      while ( counter > 0 ) {
        index = Math.floor( Math.random() * counter );
        counter--;
        temp = array[ counter ];
        array[ counter ] = array[ index ];
        array[ index ] = temp;
      }
      return array;
    }

    function word2array( string ) {
      return shuffle( string.split( "" ) );
    }

    function build( el, options ) {
      const randomwords = shuffle( options.words );
      const limit = (options.words.length > options.limit) ? options.limit : options.words.length;
      for ( let i = 0; i < limit; i++ ) {
        const word = randomwords[ i ];
        const wordarr = word2array( word );

        // prepare broken words and inputs
        let brokenWord = "";
        const brokenWordInput = [];

        for ( let k = 0; k < wordarr.length; k++ ) {
          const segment = wordarr[ k ];
          brokenWord += `<span data-char="${segment}" class=segment>${segment}</span>`;
          brokenWordInput[ k ] = $( "<input/>", {
            class: "input form-control",
            name: new Date().getTime(),
            type: "text",
            "aria-label": "input-word",
            maxlength: 1,
            size: 3
          } ).data( "answer", word[ k ] );
        }

        const label = $( "<div/>", {
          class: "single step word"
        } ).data( "answer", word ).append( brokenWord );

        const inputblock = $( "<div/>", {
          class: "guess"
        } ).data( "answer", word ).data( "range", wordarr );

        for ( let j = 0; j < brokenWordInput.length; j++ ) {
          inputblock.append( brokenWordInput[ j ] );
        }
        const $wrap = $( "<div class=\"breaker\"/>" ).append( label ).append( inputblock );
        el.append( $wrap );
      }
    }

    function handleGameComplete( el ) {
      let gameComplete = true;
      el.closest( ".codebreak" ).find( ".breaker" ).each( function() {
        if ( !$( this ).data( "complete" ) ) {
          gameComplete = false;
        }
      } );

      if ( gameComplete ) {

        if(ticker){
          clearInterval(ticker);
        }

        ticker = setTimeout( function() {
          el.blur();
          const gameblock = el.closest( ".codebreak" );
          if ( !gameblock.find( ".layover" ).length ) {  
            $('html, body').animate({
              scrollTop: gameblock.offset().top - 100,
            }, 50, function(){
              gameblock.removeClass("d-flex");
            });
            gameblock.next().removeClass("overlay-hidden");
            const cta = $( `._${gameblock.data( "options" ).id}_button` );
            const replay = $( `[data-game="#${gameblock.data( "options" ).id}"]` );
            replay.prop("disabled", false ).removeClass("btn-disabled");
            gameblock.addClass( "animation" );
            cta.prop( "disabled", false ).removeClass( "btn-disabled" );
          }
        }, 1000 );
      }
    }

    function handleWordComplete( el ) {
      let wordComplete = true;
      const inputs = el.closest( ".guess" ).find( ".input" );
      let count = 0;

      inputs.each( function( ) {
        if ( this.value.toLowerCase() !== "" ) {
          count++;
        }

        if ( !$( this ).data( "correct" ) ) {
          wordComplete = false;
        }
      } );

      // check if all inputs are filled with chacters.
      const isComplete = ( inputs.length === count );

      if ( isComplete && wordComplete ) {
        setTimeout( function() {
          if ( wordComplete ) {
            el.closest( ".breaker" ).data( "complete", true );
            el.closest( ".guess" ).find( ".input" ).prop( "disabled", true );

            if ( el.closest( ".breaker" ).next().find( ".input" ).length ) {
              el.closest( ".breaker" ).next().find( ".input" ).eq( 0 )
                .focus();
            }
            handleGameComplete( el );
          }
        }, 200 );
      } else if ( isComplete ) {
        inputs.val( "" );
        inputs.eq( 0 ).focus();
        el.closest( ".breaker" ).find( ".segment" ).removeClass( "picked" );
        el.closest( ".guess" ).shake( 3, 10, 300 );
      }
    }

    function handleKeypress( e ) {
      const _ = $( this );
      let key = e.key || e.charCode;
      key = key.toLowerCase();
      const allow = _.closest( ".breaker" )
        .find( `[data-char="${key}"]` )
        .not( ".picked" ).eq( 0 )
        .length;

      if ( !allow ) {
        e.preventDefault();
      }
    }

    function highlightCharacters( el ) {
      const wlist = el.closest( ".breaker" );
      wlist.find( ".picked" ).removeClass( "picked" );
      el.closest( ".guess" ).find( ".input" ).each( function( ) {
        const { value } = this;
        wlist
          .find( `[data-char="${value.toLowerCase()}"]` )
          .not( ".picked" )
          .eq( 0 )
          .addClass( "picked" );
      } );
    }

    function handleKeyup( event ) {
      const _ = $( this );
      highlightCharacters( _ );

      const value = _.val().toLowerCase();

      // skip for some charcodes [back and forth]
      if ( event.which === 8 || event.which === 9 || event.which === 16 ) {
        if ( event.which === 8 ) {
          _.prev( ".input" ).focus().val('');
          highlightCharacters( _ );
        }
        return true; // eslint-disable
      }

      // dont let move forward without having data.
      if ( value ) {
        _.next( ".input" ).focus().select();
      }

      const state = ( _.data( "answer" ) === value );
      _.data( "correct", state );

      setTimeout( function() {
        handleWordComplete( _ );
      }, 200 );
    }

    function handleOnclick() {
      $( this ).select();
    }

    function resetGamePlay(){
      let game = $($(this).data('game'));
      let options = game.data('options');
      $(this).prop('disabled',true).addClass("btn-disabled");
      game.html('').show();
      game.addClass('d-flex').removeClass('hidden');
      $('#'+options.id+'_success_img').addClass('overlay-hidden');
      game.codebreaker( options );
    }

    function events( el ) {
      el.on( "keydown", ".input", handleKeypress );
      el.on( "keyup", ".input", handleKeyup );
      el.on( "click", ".input", handleOnclick );
      $('.codebreaker_replay').off().on("click",resetGamePlay);
    }

    function init( el ) {
      build( el, params );
      events( el );
    }
  
    return $( this ).each( function() {
      params.words = params.words.map( v => v.toLowerCase() );
      $( this ).data( "options", params ).addClass( "codebreak" );
      init( $( this ) );
    } );
  };

  $.fn.codebreaker.defaults = {
    words: [],
    limit: 5
  };
} )( jQuery );
