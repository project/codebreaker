/* A javascript-enhanced crossword puzzle */
/* eslint-disable no-unused-vars, no-unused-vars, prefer-destructuring, no-undef */

( function( $, Drupal, drupalSettings ) {
  Drupal.behaviors.codebreaker = {
    attach( context, settings ) {
      if(context == document) {
        const datas = drupalSettings.gameBoxes.codebreaker;
        $.each(datas, function( index, data ) {
          var uniqueid = data.uniqueID;
          var words = data.words;
          var limit = data.limit;
          var successText = data.success_text;
          $( `#${uniqueid}` ).codebreaker( {
            words,
            limit,
            success: successText,
            id: uniqueid
          } );
        } );
      }
    } 
  };
} )( jQuery, Drupal, drupalSettings );
